package server;

import java.io.IOException;

public class DemoServlet extends HttpServlet{
    public void doGet(Request request, Response response) {
        String content = "<H2>this is a demo</h2>";
        try {
            response.output(HttpProtocolUtil.getHttpHeader200(content.getBytes().length) + content);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void doPost(Request request, Response response) {

    }

    public void init() throws Exception {

    }

    public void destory() throws Exception {

    }
}
