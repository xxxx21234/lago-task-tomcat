package server.classloader;


import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class MyClassloader extends ClassLoader{
    //类的路径
    private String classPath = "";
    public MyClassloader(  String classPath){
        this.classPath = classPath;
    }

    @Override
    protected Class<?> findClass(String name) {
        String myPath = classPath + "/" + name.replace(".","/") + ".class";
        System.out.println(myPath);
        byte[] cLassBytes = null;
        Path path = null;
        try {
            URI uri = new File(myPath).toURI();
            path = Paths.get(uri);
            cLassBytes = Files.readAllBytes(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Class clazz = defineClass(name, cLassBytes, 0, cLassBytes.length);
        return clazz;
    }
}
